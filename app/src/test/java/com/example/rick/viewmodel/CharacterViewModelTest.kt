package com.example.rick.viewmodel

import com.example.rick.CoroutinesTestExtension
import com.example.rick.model.RMRepo
import com.example.rick.model.entity.CharacterPage
import com.example.rick.model.remote.response.NetworkResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class CharacterViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val repo = mockk<RMRepo>()
    val vM: CharacterViewModel = CharacterViewModel(repo)

    @Test
    @DisplayName("Tests view model's state")
    fun testViewModel() = runTest(extension.dispatcher) {
        // given
        val expected = NetworkResponse.SuccessfulCharacter(
            listOf<CharacterPage>(CharacterPage(name = "Bob"))
        )
        coEvery { repo.getCharacters() } coAnswers { expected }
        // when
        vM.getCharacters()
        // then
        Assertions.assertFalse(vM.characterState.value.isLoading)
        Assertions.assertEquals(vM.characterState.value.characters, expected.characters)
    }
}

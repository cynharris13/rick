package com.example.rick.model.dto

import com.example.rick.model.remote.response.character.CurrentLocation
import kotlinx.serialization.Serializable

/**
 * Character data transfer object.
 *
 * @property episode
 * @property gender
 * @property id
 * @property image
 * @property location
 * @property name
 * @property origin
 * @property species
 * @property status
 * @property type
 * @property url
 * @constructor Create empty Character d t o
 */
@Serializable
data class CharacterDTO(
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: CurrentLocation,
    val name: String,
    val origin: CurrentLocation,
    val species: String,
    val status: String,
    val type: String,
    val url: String
)

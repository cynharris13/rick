package com.example.rick.model.entity

import kotlinx.serialization.Serializable

/**
 * Location.
 *
 * @property dimension
 * @property id
 * @property name
 * @property residents
 * @property type
 * @property url
 * @constructor Create empty Location
 */
@Serializable
data class Location(
    val dimension: String = "",
    val id: Int = 0,
    val name: String = "",
    val residents: List<String> = emptyList(),
    val type: String = "",
    val url: String = ""
)

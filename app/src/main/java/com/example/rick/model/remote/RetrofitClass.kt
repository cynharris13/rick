package com.example.rick.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create

/**
 * Local retrofit.
 *
 * @constructor Create empty Local retrofit
 */
object RetrofitClass {
    private const val BASE_URL = "https://rickandmortyapi.com/api/"
    val mediaType = "application/json".toMediaType()

    private val json = Json {
        ignoreUnknownKeys = true
    }

    @OptIn(ExperimentalSerializationApi::class)
    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(json.asConverterFactory(mediaType))
        .build()

    /**
     * Get character API.
     *
     * @return
     */
    fun getRMAPI(): RMAPI = retrofit.create()
}

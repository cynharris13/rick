package com.example.rick.model.remote

import com.example.rick.model.entity.Episode
import com.example.rick.model.entity.Location
import com.example.rick.model.remote.response.character.CharacterResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Rick and Morty API.
 *
 * @constructor Create empty Rmapi
 */
interface RMAPI {
    @GET(CHARACTER_ENDPOINT)
    fun getCharacters(): Call<CharacterResponse>

    @GET(LOCATION_ENDPOINT + LOCATION_FETCHED)
    fun getLocation(
        @Path("loc") locationName: String
    ): Call<Location>

    @GET(EPISODE_ENDPOINT + EPISODE_FETCHED)
    fun getEpisode(
        @Path("ep") episodeCode: String
    ): Call<Episode>

    companion object {
        private const val EPISODE_FETCHED = "{ep}"
        private const val LOCATION_FETCHED = "{loc}"
        private const val CHARACTER_ENDPOINT = "character/"
        private const val LOCATION_ENDPOINT = "location/"
        private const val EPISODE_ENDPOINT = "episode/"
    }
}

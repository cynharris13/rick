package com.example.rick.model.entity

import com.example.rick.model.remote.response.character.CurrentLocation

/**
 * Character page entity.
 *
 * @property episode
 * @property gender
 * @property id
 * @property image
 * @property location
 * @property name
 * @property origin
 * @property species
 * @property status
 * @property type
 * @property url
 * @constructor Create empty Character page
 */
data class CharacterPage(
    val episode: List<String> = emptyList(),
    val gender: String = "",
    val id: Int = 0,
    val image: String = "",
    val location: CurrentLocation = CurrentLocation(),
    val name: String = "",
    val origin: CurrentLocation = CurrentLocation(),
    val species: String = "",
    val status: String = "",
    val type: String = "",
    val url: String = ""
)

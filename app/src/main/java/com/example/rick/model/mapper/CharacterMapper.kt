package com.example.rick.model.mapper

import com.example.rick.model.dto.CharacterDTO
import com.example.rick.model.entity.CharacterPage

/**
 * Character mapper.
 *
 * @constructor Create empty Character mapper
 */
class CharacterMapper : Mapper<CharacterDTO, CharacterPage> {
    override fun invoke(dto: CharacterDTO): CharacterPage = with(dto) {
        CharacterPage(
            episode,
            gender,
            id,
            image,
            location,
            name,
            origin,
            species,
            status,
            type,
            url
        )
    }
}

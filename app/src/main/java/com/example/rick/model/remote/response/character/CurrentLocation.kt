package com.example.rick.model.remote.response.character

import kotlinx.serialization.Serializable

/**
 * Current location.
 *
 * @property name
 * @property url
 * @constructor Create empty Current location
 */
@Serializable
data class CurrentLocation(
    val name: String = "",
    val url: String = ""
)

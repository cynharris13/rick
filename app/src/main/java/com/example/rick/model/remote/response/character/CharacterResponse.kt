package com.example.rick.model.remote.response.character

import com.example.rick.model.dto.CharacterDTO
import kotlinx.serialization.Serializable

/**
 * Character response.
 *
 * @property results
 * @constructor Create empty Character response
 */
@Serializable
data class CharacterResponse(
    val results: List<CharacterDTO> = emptyList()
)

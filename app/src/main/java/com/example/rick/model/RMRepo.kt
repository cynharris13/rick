package com.example.rick.model

import com.example.rick.model.entity.CharacterPage
import com.example.rick.model.entity.Episode
import com.example.rick.model.entity.Location
import com.example.rick.model.mapper.CharacterMapper
import com.example.rick.model.remote.RMAPI
import com.example.rick.model.remote.response.NetworkResponse
import com.example.rick.model.remote.response.character.CharacterResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Rick and morty repository.
 *
 * @property api
 * @property mapper
 * @constructor Create empty R m repo
 */
class RMRepo(
    private val api: RMAPI,
    private val mapper: CharacterMapper
) {
    /**
     * Get characters.
     *
     * @return
     */
    suspend fun getCharacters(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val characterResponse: Response<CharacterResponse> = api.getCharacters().execute()

        return@withContext if (characterResponse.isSuccessful) {
            val pageResponse = characterResponse.body() ?: CharacterResponse()
            val characterList: List<CharacterPage> = pageResponse.results.map { mapper(it) }
            NetworkResponse.SuccessfulCharacter(characterList)
        } else {
            NetworkResponse.Error(characterResponse.message())
        }
    }

    /**
     * Get location.
     *
     * @param location
     * @return
     */
    suspend fun getLocation(location: String): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val locationResponse: Response<Location> = api.getLocation(location).execute()

        return@withContext if (locationResponse.isSuccessful) {
            val pageResponse = locationResponse.body() ?: Location()
            NetworkResponse.SuccessfulLocation(pageResponse)
        } else {
            NetworkResponse.Error(locationResponse.message())
        }
    }

    /**
     * Get episode.
     *
     * @param episode
     * @return
     */
    suspend fun getEpisode(episode: String): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val episodeResponse: Response<Episode> = api.getEpisode(episode).execute()

        return@withContext if (episodeResponse.isSuccessful) {
            val pageResponse = episodeResponse.body() ?: Episode()
            NetworkResponse.SuccessfulEpisode(pageResponse)
        } else {
            NetworkResponse.Error(episodeResponse.message())
        }
    }
}

package com.example.rick.model.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Episode(
    @SerialName("air_date")
    val airDate: String = "",
    val characters: List<String> = emptyList(),
    val episode: String = "",
    val id: Int = 0,
    val name: String = "",
    val url: String = ""
)

package com.example.rick.model.remote.response

import com.example.rick.model.entity.CharacterPage
import com.example.rick.model.entity.Episode
import com.example.rick.model.entity.Location

/**
 * Network response.
 *
 * @param T
 * @constructor Create empty Network response
 */
sealed class NetworkResponse<T> {
    /**
     * Successful character contains the retrieved list of [CharacterPage].
     *
     * @property characters
     * @constructor Create empty Successful character
     */
    data class SuccessfulCharacter(
        val characters: List<CharacterPage>
    ) : NetworkResponse<List<CharacterPage>>()

    /**
     * Successful location contains the retrieved list of [Location].
     *
     * @property location
     * @constructor Create empty Successful location
     */
    data class SuccessfulLocation(
        val location: Location
    ) : NetworkResponse<Location>()

    /**
     * Successful episode contains the retrieved list of [Episode].
     *
     * @property episode
     * @constructor Create empty Successful location
     */
    data class SuccessfulEpisode(
        val episode: Episode
    ) : NetworkResponse<Episode>()

    /**
     * Loading.
     *
     * @constructor Create empty Loading
     */
    object Loading : NetworkResponse<Unit>()

    /**
     * Error.
     *
     * @property message
     * @constructor Create empty Error
     */
    data class Error(
        val message: String
    ) : NetworkResponse<String>()
}

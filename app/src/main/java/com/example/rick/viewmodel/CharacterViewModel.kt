package com.example.rick.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.rick.model.RMRepo
import com.example.rick.model.entity.CharacterPage
import com.example.rick.model.remote.response.NetworkResponse
import com.example.rick.view.character.CharacterScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

private const val TAG = "CharacterViewModel"

/**
 * Character view model.
 *
 * @property repo
 * @constructor Create empty Character view model
 */
class CharacterViewModel(
    private val repo: RMRepo
) : ViewModel() {
    private val _characters: MutableStateFlow<CharacterScreenState> = MutableStateFlow(
        CharacterScreenState()
    )
    val characterState: StateFlow<CharacterScreenState> get() = _characters

    /**
     * Get characters.
     *
     */
    fun getCharacters() = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        when (val characters = repo.getCharacters()) {
            is NetworkResponse.Error -> _characters.update {
                it.copy(isLoading = false, error = "oh no")
            }
            is NetworkResponse.SuccessfulCharacter -> _characters.update {
                it.copy(isLoading = false, characters = characters.characters)
            }
            else -> Log.e(TAG, "getCharacters: uh oh")
        }
    }

    /**
     * Select character grabs a specific [CharacterPage] to display details of.
     *
     * @param character
     */
    fun selectCharacter(character: CharacterPage) = viewModelScope.launch {
        _characters.update { it.copy(selectedCharacter = character) }
    }

    /**
     * Get location.
     *
     * @param locationName
     */
    fun getLocation(locationName: String) = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        when (val location = repo.getLocation(locationName)) {
            is NetworkResponse.Error -> _characters.update {
                it.copy(isLoading = false, error = "oh NO")
            }
            is NetworkResponse.SuccessfulLocation ->
                _characters.update {
                    it.copy(isLoading = false, location = location.location)
                }
            else -> Log.e(TAG, "getLocation: ??")
        }
    }

    /**
     * Get episode.
     *
     * @param episodeName
     */
    fun getEpisode(episodeName: String) = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        when (val episode = repo.getEpisode(episodeName)) {
            is NetworkResponse.Error -> _characters.update {
                it.copy(isLoading = false, error = "oh NO")
            }
            is NetworkResponse.SuccessfulEpisode ->
                _characters.update {
                    it.copy(isLoading = false, episode = episode.episode)
                }
            else -> Log.e(TAG, "getEpisode: ??")
        }
    }
}

/**
 * View model factory.
 *
 * @property repo repository of [CharacterPage].
 * @constructor Create empty V m factory
 */
class VMFactory(
    private val repo: RMRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterViewModel(repo) as T
    }
}

package com.example.rick.view.episode

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.rick.model.entity.Episode

@Composable
fun EpisodeDetailsScreen(episode: Episode) {
    Column() {
        Row(
            modifier = Modifier.background(Color.Blue)
        ) {
            Text(
                text = "Name: ${episode.name}",
                fontSize = 30.sp,
                fontWeight = FontWeight.Bold
            )
        }
        Row() {
            Text(text = "Episode order: ${episode.episode}", fontSize = 25.sp)
        }
        Row() {
            Text(text = "Air date: ${episode.airDate}", fontSize = 25.sp)
        }
        Row() {
            Text(text = "URL: ${episode.url}", fontSize = 15.sp)
        }
    }
}

/*
val air_date: String = "",
    val characters: List<String> = emptyList(),
    val created: String = "",
    val episode: String = "",
    val id: Int = 0,
    val name: String = "",
    val url: String = ""
 */

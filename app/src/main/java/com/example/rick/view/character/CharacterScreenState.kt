package com.example.rick.view.character

import com.example.rick.model.entity.CharacterPage
import com.example.rick.model.entity.Episode
import com.example.rick.model.entity.Location

/**
 * Character screen state.
 *
 * @property isLoading
 * @property characters
 * @property error
 * @property selectedCharacter
 * @property location
 * @property selectedLocation
 * @property episode
 * @constructor Create empty Character screen state
 */
data class CharacterScreenState(
    val isLoading: Boolean = false,
    val characters: List<CharacterPage> = emptyList(),
    val error: String = "",
    val selectedCharacter: CharacterPage = CharacterPage(),
    val location: Location = Location(),
    val selectedLocation: String = "",
    val episode: Episode = Episode()
)

package com.example.rick.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.rick.model.RMRepo
import com.example.rick.model.mapper.CharacterMapper
import com.example.rick.model.remote.RetrofitClass
import com.example.rick.ui.theme.RickTheme
import com.example.rick.view.character.CharacterDetailsScreen
import com.example.rick.view.character.CharacterListScreen
import com.example.rick.view.episode.EpisodeDetailsScreen
import com.example.rick.view.location.LocationDetailsScreen
import com.example.rick.viewmodel.CharacterViewModel
import com.example.rick.viewmodel.VMFactory

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {
    private val characterViewModel by viewModels<CharacterViewModel> {
        val repo = RMRepo(RetrofitClass.getRMAPI(), CharacterMapper())
        VMFactory(repo)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        characterViewModel.getCharacters()
        setContent {
            val characterState by characterViewModel.characterState.collectAsState()
            RickTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "characters") {
                        composable("characters") { backStackEntry ->
                            CharacterListScreen(characterState) { selected ->
                                characterViewModel.selectCharacter(selected)
                                navController.navigate("characterDetails")
                            }
                        }
                        composable("characterDetails") {
                            CharacterDetailsScreen(
                                characterState.selectedCharacter,
                                { selected ->
                                    characterViewModel.getLocation(selected)
                                    navController.navigate("locationDetails")
                                },
                                {
                                    characterViewModel.getEpisode(it)
                                    navController.navigate("episodeDetails")
                                }
                            )
                        }
                        composable("locationDetails") {
                            LocationDetailsScreen(characterState.location)
                        }
                        composable("episodeDetails") {
                            EpisodeDetailsScreen(characterState.episode)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    RickTheme {
        Greeting("Android")
    }
}

package com.example.rick.view.character

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.rick.model.entity.CharacterPage

private const val RIGHT_WEIGHT = 5f
private const val LEFT_WEIGHT = 3f

@Composable
fun CharacterDetailsScreen(
    character: CharacterPage,
    locationClick: (String) -> Unit,
    episodeClick: (String) -> Unit
) {
    Column() {
        Row() {
            Column(modifier = Modifier.weight(LEFT_WEIGHT)) {
                AsyncImage(model = character.image, contentDescription = "Profile pic")
            }
            Column(modifier = Modifier.weight(RIGHT_WEIGHT)) {
                Text(text = character.name, fontWeight = FontWeight.Bold, fontSize = 40.sp)
            }
        }
        Row() { Text(text = "Species: ${character.species}", fontSize = 20.sp) }
        Row() { Text(text = "Gender: ${character.gender}", fontSize = 20.sp) }
        if (character.type.isNotEmpty()) {
            Row() {
                Text(text = "Type: ${character.type}", fontSize = 20.sp)
            }
        }
        Row(
            modifier = Modifier.background(Color.Magenta).clickable(
                onClick = { episodeClick(character.episode[0].filter { it.isDigit() }) }
            )
        ) {
            Text(text = "First episode: ${character.episode[0]}", fontSize = 20.sp)
        }
        Row() { Text(text = "Status: ${character.status}", fontSize = 20.sp) }
        Row(
            modifier = Modifier.background(Color.Green).clickable(
                onClick = { locationClick(character.location.url.filter { it.isDigit() }) }
            )
        ) {
            Text(text = "Current location: ${character.location.name}", fontSize = 20.sp)
        }
        Row(
            modifier = Modifier.background(Color.Cyan).clickable(
                onClick = { locationClick(character.location.url.filter { it.isDigit() }) }
            )
        ) {
            Text(
                text = "Location of origin: ${character.origin.name}\n${character.origin.url}",
                fontSize = 20.sp
            )
        }
        Row() { Text(text = "Urls: ${character.url}") }
    }
}

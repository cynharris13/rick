package com.example.rick.view.location

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.rick.model.entity.Location

@Composable
fun LocationDetailsScreen(location: Location) {
    Column() {
        Row(
            modifier = Modifier.background(Color.Blue)
        ) {
            Text(text = "Location: ${location.name}", fontSize = 30.sp, fontWeight = FontWeight.Bold)
        }
        Row() {
            Text(text = "Dimension: ${location.dimension}", fontSize = 25.sp)
        }
        Row() {
            Text(text = "Type: ${location.type}", fontSize = 25.sp)
        }
        /*
        Row(modifier = Modifier.background(Color.LightGray)) {
            Text(text = "Residents:", fontSize = 20.sp)
            for(resident in location.residents) {
                Text(text = resident)
            }
        }
         */
        Row() {
            Text(text = "URL: ${location.url}", fontSize = 20.sp)
        }
    }
}

/*
 val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
 */
